import { Container } from "reactstrap";
import './App.css'
import Drink from "./components/content/DrinkComponent";
import Form from "./components/content/FormComponent";
import Introduce from "./components/content/IntroduceComponent";
import Lading from "./components/content/Lading";
import Size from "./components/content/SizeComponent";
import Submit from "./components/content/Submit";
import Type from "./components/content/TypeComponent";
import Footer from "./components/footer/footer";
import Header from "./components/header/header";



function App() {
  return (
  <Container>
    {/* <!-- 1.1 Header - Navbar menu --> */}
    <Header/>
    {/* Carousel */}
    <Lading/>
    <Introduce/>
    <Size/>
    <Type/>
    <Drink/>
    <Form/>
    <Submit/>
    <Footer/>
  </Container>
  );
}

export default App;
