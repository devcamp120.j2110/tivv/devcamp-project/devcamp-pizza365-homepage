function Form() {
    return (
        <>
            <p className="text-center h3 text-warning">Gửi đơn hàng </p>
            <hr className="border-warning" />
            <form>
                <div className="row py-4">
                    <div className="col-12">
                        <label htmlFor="">Tên</label>
                        <input type="text" className="form-control w-100" placeholder="Nhập tên"></input>
                        <label htmlFor="">Email</label>
                        <input type="text" className="form-control w-100" placeholder="Nhập email"></input>
                        <label htmlFor="">Địa chỉ</label>
                        <input type="text" className="form-control w-100" placeholder="Nhập số điện thoại"></input>
                        <label htmlFor="">Số điện thoại</label>
                        <input type="text" className="form-control w-100" placeholder="Nhập địa chỉ"></input>
                        <label htmlFor="">Mã giảm giá</label>
                        <input type="text" className="form-control w-100" placeholder="Nhập mã giảm giá"></input>
                        <label htmlFor="">Lời nhắn</label>
                        <input type="text" className="form-control w-100" placeholder="Nhập lời nhắn"></input>
                        
                    </div>
                </div>
            </form>
        </>
    )
}
export default Form;