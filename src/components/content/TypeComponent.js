
import seafoodImage from "../../assets/images/seafood.jpg"
import hawaiiImage from "../../assets/images/hawaiian.jpg"
import baconImage from "../../assets/images/bacon.jpg"

function Type() {
    return (
        <>
            <p className="text-center h3 text-warning pt-5">Chọn loại Pizza </p>
            <hr className="border-warning" />
            <div className="row pt-4">
                <div className="col-4">
                    <div className="card">
                        <img className="card-img-top" src={seafoodImage} alt="" />
                        <div className="card-body">
                            <h4 className="card-title font-weight-bold">OCEAN MANIA</h4>
                            <p className="card-text h6">PIZZA HẢI SẢN SỐT MAYONNAISE</p>
                            <p className="card-text">Sốt cà chua, phô mai, tôm, mực, thanh cua, hành tây.</p>
                            <button className="btn btn-warning w-100 font-weight-bold">Chọn</button>
                        </div>
                    </div>
                </div>
                <div className="col-4">
                    <div className="card">
                        <img className="card-img-top" src={hawaiiImage} alt="" />
                        <div className="card-body">
                            <h4 className="card-title font-weight-bold">HAWAIIAN</h4>
                            <p className="card-text h6">PIZZA DĂM BÔNG DỨA KIỂU HAWAII</p>
                            <p className="card-text">Sốt phô mai, cà chua, thịt dăm bông thơm.</p>
                            <button className="btn btn-warning w-100 font-weight-bold">Chọn</button>
                        </div>
                    </div>
                </div>
                <div className="col-4">
                    <div className="card">
                        <img className="card-img-top" src={baconImage} alt="" />
                        <div className="card-body">
                            <h4 className="card-title font-weight-bold">CHEESY CHICKEN BACON</h4>
                            <p className="card-text h6 fa-flip-horizontal">PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI</p>
                            <p className="card-text">Sốt phô mai, thịt gà, thịt heo muối, phô mai, cà chua.</p>
                            <button className="btn btn-warning w-100 font-weight-bold">Chọn</button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )}
export default Type;