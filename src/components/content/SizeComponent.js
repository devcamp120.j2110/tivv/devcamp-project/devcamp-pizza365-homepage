
function Size () {
    return (
        <>
            <p className="text-center h3 text-warning pt-5">Chọn size Pizza </p>
            <hr className="border-warning" />
            <p className="text-center text-warning">Chọn combo Pizza phù hợp với nhu cầu của bạn </p>
            <div className="row">
                <div className="col-4 p-2 border-warning">
                    <div className="card">
                        <div className="card-header header-01 text-center font-weight-bolder">
                            S (small)
                        </div>
                        <div className="card-body text-center">
                            <p className="card-text">Đường kính: <span>20cm</span></p>
                            <hr />
                            <p className="card-text">Sườn nướng: <span>2</span></p>
                            <hr />
                            <p className="card-text">Salad: <span>200g</span></p>
                            <hr />
                            <p className="card-text">Nước ngọt: <span>2</span></p>
                            <hr />
                            <p className="card-text"><span className="h1">150.000</span><br />VNĐ</p>
                        </div>
                        <div className="card-footer text-muted">
                            <button className="btn btn-warning w-100 font-weight-bold">Chọn</button>
                        </div>
                    </div>
                </div>
                <div className="col-4 p-2 border-warning">
                    <div className="card">
                        <div className="card-header header-02 text-center font-weight-bolder">
                            M (medium)
                        </div>
                        <div className="card-body text-center">
                            <p className="card-text">Đường kính: <span>25cm</span></p>
                            <hr />
                            <p className="card-text">Sườn nướng: <span>4</span></p>
                            <hr />
                            <p className="card-text">Salad: <span>300g</span></p>
                            <hr />
                            <p className="card-text">Nước ngọt: <span>3</span></p>
                            <hr />
                            <p className="card-text"><span className="h1">200.000</span><br />VNĐ</p>
                        </div>
                        <div className="card-footer text-muted">
                            <button className="btn btn-warning w-100 font-weight-bold">Chọn</button>
                        </div>
                    </div>
                </div>
                <div className="col-4 p-2 border-warning">
                    <div className="card">
                        <div className="card-header header-03 text-center font-weight-bolder">
                            L (large)
                        </div>
                        <div className="card-body text-center">
                            <p className="card-text">Đường kính: <span>30cm</span></p>
                            <hr />
                            <p className="card-text">Sườn nướng: <span>8</span></p>
                            <hr />
                            <p className="card-text">Salad: <span>500g</span></p>
                            <hr />
                            <p className="card-text">Nước ngọt: <span>4</span></p>
                            <hr />
                            <p className="card-text"><span className="h1">250.000</span><br />VNĐ</p>
                        </div>
                        <div className="card-footer text-muted">
                            <button className="btn btn-warning w-100 font-weight-bold">Chọn</button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default Size;