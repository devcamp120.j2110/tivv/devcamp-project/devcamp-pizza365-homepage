function Drink(){
    return (
        <>
            <p className="text-center h3 text-warning pt-5">Chọn đồ uống </p>
            <hr className="border-warning" />
            <div className="row p-3">
                <select name="drink" id="sel-drink" className="form-control">
                    <option value="All">Tất cả loại nước uống</option>
                    <option value="0">Trà tắc</option>
                    <option value="1">Pepsi</option>
                    <option value="2">CocaCola</option>
                    <option value="3">Trà sữa</option>
                    <option value="4">Cà phê</option>
                </select>
            </div>
        </>
    )
}
export default Drink;
