function Introduce () {
    return (
        <>
            <p className="text-center h3 text-warning">Tại sao lại Pizza 365 </p>
            <hr className="border-warning" />
            <div className="row p-2">
                <div className="col-3 intro-1">
                    <div>
                        <h4 className="font-weight-bold py-3">Đa dạng</h4>
                    </div>
                    <div>
                        <p>Số lượng Pizza đa dạng, có đầy đủ các loại Pizza đang hot nhất hiện nay</p>
                    </div>
                </div>
                <div className="col-3 intro-2">
                    <div>
                        <h4 className="font-weight-bold py-3">Chất lượng</h4>
                    </div>
                    <div>
                        <p>Nguyên liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo vệ sinh an toàn thực phẩm</p>
                    </div>
                </div>
                <div className="col-3 intro-3">
                    <div>
                        <h4 className="font-weight-bold py-3">Hương vị</h4>
                    </div>
                    <div>
                        <p>Đảm bảo hương vị ngon, độc, lạ mà bạn chỉ có thể trải nghiệm được từ Pizza 565</p>
                    </div>
                </div>
                <div className="col-3 intro-4">
                    <div>
                        <h4 className="font-weight-bold py-3">Dịch vụ</h4>
                    </div>
                    <div>
                        <p>Nhân viên thân thiện, nhà hàng hiện đại. Dịch vụ giao hàng nhanh chất lượng, tân tiến.</p>
                    </div>
                </div>
            </div>
        </>
    )
}
export default Introduce;
