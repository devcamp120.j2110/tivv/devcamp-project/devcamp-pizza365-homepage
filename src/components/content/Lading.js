import image1 from '../../assets/images/1.jpg'
import image2 from '../../assets/images/2.jpg'
import image3 from '../../assets/images/3.jpg'
import image4 from '../../assets/images/4.jpg'
import Carousel from '../../../node_modules/react-bootstrap/Carousel'
import { Container } from 'reactstrap'
function Lading () {
    return (
        <Container className='mt-5'>
        <h1 className="text-warning text-uppercase font-weight-bold pt-5">Pizza 365</h1>
        <h6 className="text-warning font-italic">Truly Italian</h6>
        <Carousel>
            <Carousel.Item>
                <img className="d-block w-100" src={image1} alt="First slide" />
            </Carousel.Item>
            <Carousel.Item>
                <img className="d-block w-100" src={image2} alt="Second slide" />
            </Carousel.Item>
            <Carousel.Item>
                <img className="d-block w-100" src={image3} alt="Third slide" />
            </Carousel.Item>
            <Carousel.Item>
                <img className="d-block w-100" src={image4} alt="fourth slide" />
            </Carousel.Item>
        </Carousel>
    </Container>
    )
}
export default Lading;