function Footer() {
    return (
        <footer>
            <div className="bg-warning text-center">
                <p className="font-weight-bold">Footer</p>
                <a href="#"><button className="btn btn-secondary"><i className="fas fa-arrow-up"></i> To the top</button></a>
                <p><i className="fab fa-facebook-square"></i>
                    <i className="fab fa-instagram"></i>
                    <i className="fab fa-snapchat"></i>
                    <i className="fab fa-pinterest-p"></i>
                    <i className="fab fa-twitter"></i>
                    <i className="fab fa-linkedin-in"></i>
                </p>
                <p className="font-weight-bolder">Powered by DEVCAMP</p>
            </div>
        </footer>
    )
}
export default Footer;