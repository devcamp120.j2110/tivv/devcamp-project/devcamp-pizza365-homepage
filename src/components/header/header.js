import 'bootstrap/dist/css/bootstrap.min.css'
import { Row, Col, Nav,List, ListInlineItem } from "reactstrap";
function Header() {
    return (
        <Row>
            <Col sm='12'>
                <Nav className="navbar navbar-expand-lg navbar-light bg-warning fixed-top">
                    <div className="collapse navbar-collapse" id="collapsibleNavId">
                        <List  className="navbar-nav mr-auto mt-2 mt-lg-0 nav-fill w-100">
                            <ListInlineItem className="nav-item active">
                                <a className="nav-link" href="#">Trang chủ </a>
                            </ListInlineItem>
                            <ListInlineItem className="nav-item">
                                <a className="nav-link" href="#">Combo</a>
                            </ListInlineItem>
                            <ListInlineItem className="nav-item">
                                <a className="nav-link" href="#">Loại Pizza</a>
                            </ListInlineItem>
                            <ListInlineItem className="nav-item">
                                <a className="nav-link" href="#">Gửi đơn hàng</a>
                            </ListInlineItem>
                        </List>
                    </div>
                </Nav>
            </Col>
        </Row>
    )
}
export default Header;